git-deploy(1)
=============

NAME
----
git-deploy - Update a live server's config from a git repository

SYNOPSIS
--------
[verse]
'git deploy' [--verbose] [--diff|--lint|--get-rm|--get-stat]
'git deploy' [--verbose] [--forward|--revert|--switch] [--clobber] <target>

DESCRIPTION
-----------
The `git deploy` command helps you to use git for system configuration
management. It does a similar job to `git checkout` but differs in the
following ways:

* It can set ownerships and permissions of files and directories, as
  specified in plain text `.gitdeploy` configuration scripts. See
  linkgit:gitdeploy[5] for details of the file format.

* It updates files and symlinks safely. In particular, it renames them
  into place so that a live server will not observe any absent or
  partially-written files. It sets ownerships and permissions between
  writing and installing the file so there are no security-related
  race conditions.

* It cannot perform merges. It requires that the live configuration is
  unmodified since the last `git deploy` or `git checkout`. This
  avoids problems caused by merge conflicts appearing in a live
  configuration.

Without any options, `git deploy` will fast-forward the current HEAD
to match the target revision, and fails if the target isn't a
descendent of the current revision.

OPTIONS
-------
--clobber::
	By default, `git deploy` will not overwrite modified or
	untracked files. This option allows you to force it to go
	ahead, losing any modifications.

--diff::
	Check the `.gitdeploy` scripts in the same way as the `--lint`
	option, and also display differences between ownerships and
	permissions as specified by the `.gitdeploy` scripts and those
	in the working tree. Only meaningful in a live working tree.

--get-rm::
	Add `rm` commands to `.gitdeploy` scripts for files that
	are to be removed in the next commit. Can be used in a
	`pre-commit` hook. Can be used in a non-privileged
	development working tree.

--get-stat::
	Create `.gitdeploy` scripts based on ownerships and
	permissions in a live working tree. Useful when making the
	initial import of a configuration.

--lint::
	Verify that the `.gitdeploy` scripts are syntactically correct
	and are consistent with presence and absence of filesystem
	objects in the git index. Can be used in a non-privileged
	development working tree.

--forward::
	Fast-forward to the target revision. This is the default.

--revert::
	Revert to a previous revision. The target must be an ancestor
	of the current HEAD.

--switch::
	Switch the current HEAD to the target revision even if it is
	not a fast-forward or a revert.

--verbose::
	Explain what's happening in more detail.

EXAMPLES
--------
The `git deploy` program is only a small part of a system
configuration management setup. Most of the heavy lifting is done
using standard git commands, including distributing configurations
over the network, examining differences between configurations, and
composing package configurations into system configurations.

Configuration changes can be divided into roughly three levels:
re-install from scratch; software upgrade; and incremental changes.
The first is handled by your OS's installer (`jumpstart`, `kickstart`,
etc.). The last is the main purpose of `git deploy`. The middle case
is usually a matter of driving the OS package tool (`dpkg`, `rpm`,
etc.), which is not `git deploy`'s job. However you may instead choose
to distribute your locally-developed packages using `git` rather than as
OS packages.

The following is a fairly elaborate setup, suitable for managing
several classes of machine.

Divide the configuration into modules, roughly corresponding to
software packages. Each module has its own branch and these branches
will usually be in separate git repositories. Modules are laid out to
match the installed layout, with top level directories of `etc`,
`bin`, `usr`, `opt`, `home`, etc. There is a base OS module for each
version of each operating system you run, which contains
customizations common to all your installations of that OS. There is a
configuration module for each configuration of each optional package;
for example you might have two configurations for BIND, one for a
cacheing recursive nameserver and one for an authoritative
nameserver. Some modules will be common to several classes of machine
and some will be class-specific.

The configuration for a class of machines is formed by merging all the
relevant modules into a single configuration tree. Each class of
machines will have its own branch and these branches will usually be
in separate git repositories. If your systems have been decomposed
into modules well, there should usually be little overlap and
therefore little opportunity for conflicts in the merge. You may need
to make some class-specific customizations in this merged
configuration. There's scope for different administration styles in
the extent to which you move class-specific configuration into modules
as opposed to doing much of it in the merged tree.

In some cases you can minimise the amount of machine-specific
configuration so that it remains static after installation. In this
case you can simply push the configuration repository to the target
machine and run `git deploy` on the class branch. In more complicated
cases you will have a branch for each machine derived from the class
branch. It's probably convenient to include all these branches in the
same repository so that you can push the same data to all the machines
in a class and run `git deploy $(hostname)` on each one.

To update a configuration, commit the change to the module's branch;
merge the branch into the relevant class branches; if necessary, merge
the class into each machine branch; push the updated configuration
repository to each machine; run `git deploy` on each machine.

The author's systems do not use the OS packaging tools for locally
developed packages. They are installed in versioned paths so that
multiple versions can be installed at the same time for quick upgrade
and roll-back. Each package is split into a build-time configuration
which creates a versioned install, and a run-time configuration in
which incremental changes are made. The run-time configuration can be
merged into the versioned install tree using git's subtree merge
logic, so its paths do not need to be changed when the package is
upgraded.

Managing removals
~~~~~~~~~~~~~~~~~
When you update a configuration that is already managed by git, git's
own removal handling is sufficient. However, when you deploy a
configuration on top of a default operating system install, there may
be some files in the installation that should be removed. This is the
purpose of the `.gitdeploy` `rm` command.

You need to keep your `rm` commands in sync when changing a
configuration module that is based on a default operating system
configuration. The module might be a base OS module covering all of
`/etc` for all the standard packages, or it might cover an OS vendor
package that isn't installed on all your machines. When you remove a
file from your configuration that was originally installed by an OS
package, you should add an `rm` command to the relevant `.gitdeploy`
file. The `git deploy --get-rm` command can do this for you. Run it
before committing your change, or add it to your pre-commit hook.

If you intend your locally produced packages to be used with `git
deploy`, they should install little or no configuration to minimize
the need to maintain `rm` commands.

Importing a configration
~~~~~~~~~~~~~~~~~~~~~~~~
The `git deploy --get-stat` command helps when importing a
configuration into git, for example when creating a base operating
system configuration module.

------------------------------------------------------------
    cd /etc
    git init --shared=0700
    git add .
    git deploy --get-stat
    git add .
    git commit -m 'Base configuration for Foonix 2009.'
------------------------------------------------------------

In the above, we take care to hide the contents of the `.git`
directory from prying eyes. The first `git add` tells git to track the
whole of `/etc`, including which files `git deploy` should check the
permissions of. The second `git add` tells git to track the new
`.gitdeploy` scripts.

HOOKS
-----
The `post-deploy` hook is run after `git deploy` has updated the
working tree. The hook is given two parameters: the ref of the
previous HEAD and the ref of the new HEAD. It can be used to notify
daemons of configuration changes.

SEE ALSO
--------
linkgit:gitdeploy[5]

Author
------
Written by Tony Finch <dot@dotat.at>
// You may do anything with this, at your own risk.
// http://creativecommons.org/publicdomain/zero/1.0/

GIT
---
Part of the linkgit:git[1] suite
