up:
	if test $$(git rev-parse HEAD) = $$(git merge-base HEAD origin/master); \
	then git merge origin/master; fi
	git gc
	git update-server-info
	rsync --delete --recursive --links .git/ chiark:public-git/git-deploy.git/
