#!/usr/bin/perl
#
# Deploy a live server's configuration from a git repository.
#
# Written by Tony Finch <dot@dotat.at>
# You may do anything with this, at your own risk.
# http://creativecommons.org/publicdomain/zero/1.0/

use warnings;
use strict;

use Errno;
use File::Copy;
use IO::Handle;

use Git;
my $git; # will contain the Git module's state object.

########################################################################
#
#  command line processing
#

my $DEBUG;
my $VERBOSE;
sub aargh { die "@_\n"; }
sub utter { print STDERR "@_\n"; }
sub debug { utter @_ if $DEBUG }
sub verbose { utter @_ if $VERBOSE }

sub usage () {
	die <<EOM;
usage:	git deploy [--verbose] [--diff|--lint|--get-rm|--get-stat]
	git deploy [--verbose] [--forward|--revert|--switch] [--clobber] <target>

Update the working tree to the target revision safely (by renaming
files into place) and adjust ownerships and permissions according to
the contents of .gitdeploy scripts.

Options:
  --lint     Verify that the .gitdeploy scripts make sense.
  --diff     Show diffs between .gitdeploy scripts and working dir.
  --get-rm   Update .gitdeploy scrips with removed files.
  --get-stat Create .gitdeploy scripts based on working tree.
  --verbose  Explain what's happening in more detail.
EOM
}

sub set_option ($$) {
	usage if defined $_[0];
	$_[0] = $_[1];
}

# main program
do {
	my $action;
	my $target;
	my $branch;
	my $clobber;
	my %option = (
		# general options
		'--debug'    => sub { $DEBUG = 1 },
		'--verbose'  => sub { $VERBOSE = 1 },
		# special actions
		'--diff'     => sub { set_option $action, \&main_diff },
		'--get-rm'   => sub { set_option $action, \&main_get_rm },
		'--get-stat' => sub { set_option $action, \&main_get_stat },
		'--lint'     => sub { set_option $action, \&main_lint },
		# deploy modes
		'--forward'  => sub { set_option $branch, 'forward' },
		'--revert'   => sub { set_option $branch, 'revert' },
		'--switch'   => sub { set_option $branch, 'switch' },
		'--clobber'  => sub { set_option $clobber, 1 },
	);
	for my $arg (@ARGV) {
		my $opt = $option{$arg};
		if (defined $opt) {
			&$opt();
		} else {
			# unrecognized options are a target revision
			set_option $target, $arg;
		}
	}
	usage unless defined $action xor defined $target;
	$branch = 'forward' unless defined $branch;

	# Find repository and move to top directory.
	$git = Git->repository();
	$git->wc_chdir(".");

	if (defined $action) {
		exit &$action();
	} else {
		exit &deploy($branch, $clobber, $target);
	}
};

########################################################################
#
#  .gitdeploy parser
#

sub load_common {
	my $usage = "usage: " . shift;
	my $map = shift;
	my $opts = shift;
	return if @_ == 0;
	return $usage if @_ > 1;
	return $usage unless exists $map->{$_[0]};
	$opts->{$map->{$_[0]}} = 1;
}

# We use the -RP or -h options to chown and chmod to avoid following symlinks.

sub load_chown {
	my $opts = shift;
	my $owner = pop;
	my $usage = load_common
	    "chown [-RP|-h] <user> <path>", {
		'-RP' => 'chown_r',
		'-R'  => 'chown_r',
		'-h'  => 'chown_h',
	}, $opts, @_;
	return $usage if defined $usage;
	if ($owner =~ /^[0-9]+$/) {
		$opts->{chown} = $owner;
	} else {
		my $uid = getpwnam $owner;
		return "chown: unknown user $owner"
		    unless defined $uid;
		$opts->{chown} = $uid;
	}
}

sub load_chgrp {
	my $opts = shift;
	my $group = pop;
	my $usage = load_common
	    "chgrp [-RP|-h] <group> <path>", {
		'-RP' => 'chgrp_r',
		'-R'  => 'chgrp_r',
		'-h'  => 'chgrp_h',
	}, $opts, @_;
	return $usage if defined $usage;
	if ($group =~ /^[0-9]+$/) {
		$opts->{chgrp} = $group;
	} else {
		my $gid = getgrnam $group;
		return "chgrp: unknown group $group"
		    unless defined $gid;
		$opts->{chgrp} = $gid;
	}
}

# Standard chmod doesn't support -RP or -h so we have to warn against them
# in the lint checker.

sub load_chmod {
	my $opts = shift;
	my $mode = pop;
	my $usage = load_common
	    "chmod [-R] <mode> <path>", {
		'-RP' => 'chmod_r',
		'-R'  => 'chmod_r',
		'-h'  => 'chmod_h',
	}, $opts, @_;
	return $usage if defined $usage;
	return "bad file mode" if $mode !~ /0[0-7]*/;
	$opts->{chmod} = $mode;
}

# The lint checker has to want against conflicts between rm and the other operations.

sub load_rm {
	my $opts = shift;
	my $arg = shift;
	return "usage: rm [-Rf|-f] path"
	    unless @_ == 0
	    and $arg =~ /^(-[Rr]f|-f)$/;
	$opts->{rm_r} = 1 if $arg =~ /[Rr]/;
	$opts->{rm} = 1;
}

my %load_command = (
	chgrp => \&load_chgrp,
	chmod => \&load_chmod,
	chown => \&load_chown,
	rm    => \&load_rm,
);

sub load_conf ($$;$) {
	my $hash = shift;
	my $dir = shift;
	my $file = shift;
	$file = "$dir/.gitdeploy" unless defined $file;
	open my $f, "<", $file
	    or aargh "open $file: $!";
	my $num = 0;
	while (defined(my $line = $f->getline)) {
		$num++;
		next if $line =~ m{^\s*(#|$)};
		my @line = split ' ', $line;
		if (@line < 3) {
			utter "$file:$num: not enough arguments";
			next;
		}
		my $target = pop @line;
		if ($target =~ m{^/|^[.][.]/|/[.][.]/}) {
			utter "$file:$num: path must not refer to parent dirs";
			next;
		}
		# canonicalize
		$target =~ s{/+}{/}g;
		$target =~ s{(^|/)[.]/}{/}g;
		my $command = $load_command{shift @line};
		if (not defined $command) {
			utter "$file:$num: unrecognized command";
			next;
		}
		my $path = "$dir/$target";
		my $opts = $hash->{$path};
		$opts = $hash->{$path} = {} unless defined $opts;
		my $error = &$command($opts, @line);
		utter "$file:$num: $error" if $error;
	}
	return $hash;
}

########################################################################
#
#  .gitdeploy interpreter
#

# Recursively delete a directory tree.
#
# Note we can't use File::Path::rmtree because it follows symlinks.
#
sub deltree {
	find {
		bydepth => 1,
		follow => 0,
		no_chdir => 1,
		wanted => sub {
			if (-d) {
				rmdir or utter "deltree rmdir $_: $!"
			} else {
				unlink or utter "deltree unlink $_: $!"
			}
		}
	}, @_;
}

# Set a file's ownerships and permissons. First argument is the .gitdeploy
# data. Second argument is the target file name, which is used to look up
# perms in the .gitdeploy data. Third argument is the temporary file name
# whose perms should be set before it is moved to the target location. If
# the third argument is missing then the target file's mode is changed
# directly.
#
sub setperms ($$;$) {
	my $info = shift;
	my $path = shift;
	my $target = shift;
	$target = $path unless defined $target;
	my $uid = $<;
	my $gid = $(;
	my $mode = 0666 & ~umask;
	my $rm;
	# look for .gitdeploy entries that affect this path,
	# working from the root towards the leaf
	my @path = split m{/}, $path;
	my $key = shift @path;
	while (@path) {
		if (my $entry = $info->{$key}) {
			$uid = $entry->{chown} if $entry->{chown_r};
			$gid = $entry->{chgrp} if $entry->{chgrp_r};
			$mode = $entry->{chmod} if $entry->{chmod_r};
			$rm = 1 if $entry->{rm_r};
		}
		$key .= '/' . shift @path;
	}
	# $key eq $path
	if (my $entry = $info->{$key}) {
		$uid = $entry->{chown} if $entry->{chown_h};
		$gid = $entry->{chgrp} if $entry->{chgrp_h};
		$mode = $entry->{chmod} if $entry->{chmod_h};
		$rm = 1 if $entry->{rm};
	}
	unless (chown $uid, $gid, $target) {
		utter "chown $uid chgrp $gid $path: $!";
	}
	unless (chmod $mode, $target) {
		utter "chmod $mode $path: $!";
	}
	if ($rm) {
		deltree $path;
	}
}

########################################################################
#
#  intefaces to git plumbing
#

# Obtain and parse output from git-ls-files.
#
sub git_list (@) {
	# -z = zero byte separators
	# -v = include full status tag
	# --stage = include full details
	# --exclude-standard = use .gitignore et al.
	my $out = $git->command
	    (qw(ls-files -z -v --stage --exclude-standard), @_);
	my $hash;
        while (length $out) {
		# Parse chunk from list.
		$out =~ s{(?i:[CHKMR?])[ ]
			  ([0-7]{6})[ ]
			  ([0-9A-Za-z]{40})[ ]
			  ([0AB])\t
			  ([^\0]*)\0
			 }{}sx
		    or aargh "Failed to parse output from git-ls-files";
		$hash->{$5} = {
			tag => $1,
			mode => $2,
			sha1 => $3,
			stage => $4,
			name => $5,
		};
	}
	return $hash;
}

# Obtain and parse output from git-diff-*.
#
# We're only interested in differences at the file level, not patches.
# We don't bother with rename or copy detection since our basic
# mechanisms handle those cases adequately. (A renamed file is
# rewritten in its new place then deleted from its old place.)
#
sub git_list_diff (@) {
	# -z = zero byte separators
	# --no-abbrev = full sha1 object IDs
	my $type = shift;
	my $out = $git->command
	    ("diff-$type", qw(-z --no-abbrev --no-renames), @_);
	my $hash;
        while (length $out) {
		# Parse chunk from list.
		$out =~ s{[:]([0-7]{6})
			  [ ]([0-7]{6})
			  [ ]([0-9A-Za-z]{40})
			  [ ]([0-9A-Za-z]{40})
			  [ ]([ACDMRTUX])
			  \0([^\0]*)\0
			 }{}x
		    or aargh "Failed to parse output from git-diff";
		my $type;
		$hash->{$6} = {
			src_mode => $1,
			dst_mode => $2,
			src_sha1 => $3,
			dst_sha1 => $4,
			type => $type = $5,
			name => $6,
		};
		$type =~ /[ADMT]/
		    or aargh "Unknown change type $type";
		# unknown types include copies and renames
		# (which we don't ask git to detect)
		# and unmerged files.
	}
	return $hash;
}

# Checkout a selection of files under temporary names.
# Return the mapping from filename to temporary name.
#
sub git_checkout_temp (@) {
	my ($pid, $from_pipe, $to_pipe, $ctx) =
	    $git->command_bidi_pipe(qw( checkout-index -z --temp --stdin ));
	$to_pipe->print(map "$_\0", @_);
	$to_pipe->close();
	local $/ = undef;
	my $out = <$from_pipe>;
	$git->command_close_bidi_pipe($pid, $from_pipe, $to_pipe, $ctx);
	my $hash;
	while (length $out) {
		# Parse chunk from list.
		$out =~ s{^([^\t]+)\t
			   ([^\0]*)\0
			 }{}sx
		    or aargh "Failed to parse output from git-checkout-index";
		my ($tempname,$filename) = ($1,$2);
		$hash->{$filename} = $tempname;
	}
	return $hash;
}

# Get the exit status from a command.
# (Sheesh, this shouldn't be so complicated!)
#
sub git_succeeds (@) {
	try {
		$git->command_noisy(@_);
	} catch Git::Error::Command with {
		my $E = shift;
		return $E->value() == 0;
	};
	return 1;
}

########################################################################
#
#  ancillary actions
#

sub main_diff () {
	die "diff"
}

sub main_get_rm () {
	die "get-rm"
}

sub main_get_stat () {
	die "get-stat"
}

sub main_lint () {
	die "lint"
}

########################################################################
#
#  git deploy
#

sub deploy ($$) {
	my $branch = shift;
	my $clobber = shift;
	my $target = shift;

	# Ensure that current working copy is unmodified.
	# (This tool does not do merges.)

	aargh "git update-index --refresh failed"
	    unless $clobber or git_succeeds qw(git update-index --refresh);
	aargh "Your working tree has been modified"
	    unless $clobber or git_succeeds qw(git diff-files --quiet);
	aargh "Your index has been modified"
	    unless $clobber or git_succeeds qw(git diff-index --quiet --cached HEAD);

	# Ensure that this is a fast-forward or revert.

	my $sha1_head = $git->command('rev-parse', 'HEAD');
	my $sha1_targ = $git->command('rev-parse', $target);
	my $sha1_base = $git->command('merge-base', 'HEAD', $target);

	if ($sha1_base eq $sha1_head) {
		utter "Fast forward to target $sha1_targ";
		aargh "Aborting because you asked for a revert"
		    if $branch eq "revert";
	} elsif ($sha1_base eq $sha1_targ) {
		utter "Revert to target $sha1_targ";
		aargh "Aborting because you asked for a fast foward"
		    if $branch eq "forward";
	} elsif ($branch eq "switch") {
		utter "Switch to unrelated target $sha1_targ";
	} else {
		aargh "Must use -f to switch to unrelated target $sha1_targ";
	}

	# Get changed files between current working copy and the target
	# revision.
	#
	# We normally use git-diff-tree to directly compare the tree
	# objects and therefore avoid scanning the working tree again.
	# (We already did so above to verify it is clean.)
	#
	# In clobber mode, we compare the target against the working tree
	# so that we can splat any modifications. We want a reverse diff
	# because we want to change the working tree into the target.

	my $diff = $clobber ? git_list_diff "index", "-R", $sha1_targ
			    : git_list_diff "tree", $sha1_head, $sha1_targ;

	# Check that no untracked files are going to be clobbered.

	# XXX: use git ls-files -o to get untracked files
	#                   and -i to get ignored files

	unless ($clobber) {
		for my $file (keys %$diff) {
			next unless $diff->{$file}->{type} eq 'A';
			if (stat $file) {
				aargh "Untracked path would be clobbered: $file";
			} elsif ($!{EISDIR} || $!{ENOTDIR}) {
				aargh "Untracked path would be clobbered: $file";
			} elsif ($!{ENOENT}) {
				# ok
			} else {
				aargh "stat $file: $!";
			}
		}
	}

	# Update index to target revision.

	$git->command_noisy('read-tree', $sha1_targ);

	# Checkout new objects under temporary filenames.

	my $tempname = git_checkout_temp(keys %$diff);

	# Load ownerships and permissions.
	#
	# While we are doing this we discover which configuration files
	# have changed, in which case we need to adjust the ownerships and
	# permissions of objects affected by this change whether or no they
	# are in the $diff changeset.
	#
	# We sort the pathnames so that we process them depth-first.

	my $perms;
	for my $conf (sort { length $b <=> length $a }
		      keys %{git_list qw( .gitdeply */.gitdeploy )}) {
		$conf =~ m{^(?:(.*)/)?[^/]$};
		my $dir = $1 || ".";
		# Unchanged .gitdeploy files are easy.
		unless (exists $tempname->{$conf}) {
			load_conf $perms, $dir;
			next;
		}
		# Apply changes in this file to the whole subtree.
		load_conf $perms, $dir, $tempname->{$conf};
		for my $path (sort { length $b <=> length $a }
			      keys %{git_list $dir}) {
			setperms $perms, $path;
		}
	}

	# Deal with directory / other conflicts.

	# Set permissions and rename new/replacement objects into place.
	#
	# We do this in two phases: first fix the temp files' permissions
	# and move them into their destination directories (or copy if we
	# get EXDEV); second rename them to their final names. The aim is
	# to try to do all the failure-prone stuff before changing live
	# files. We could simplify the first phase if `git checkout-index
	# --temp` were changed to create temp files in the right
	# subdirectories.

	for my $file (keys %{$diff}) {
		my $dir = dirname $file;
		my $temp = $tempname->{$file};
		my $dest = $tempname->{$file} = "$dir/$temp";
		setperms $perms, $file, $dest;
		next if $dir eq '.'; # file is in top level
		next if rename $temp, $dest;
		aargh "rename $temp -> $dest: $!"
		    unless $! == EXDEV;
		open my $to, ">", $dest
		    or aargh "open > $dest: $!";
		setperms $perms, $file, $dest;
		copy $temp, $to
		    or aargh "copy $temp -> $dest: $!";
		close $to or aargh "close $dest: $!";
	}

	for my $file (keys %{$diff}) {
		my $temp = $tempname->{$file};
		rename $temp, $file
		    or utter "rename $temp -> $file: $!";
	}

	# Unlink removed objects.

	# Refresh index.

	# Update HEAD.
}

# eof
