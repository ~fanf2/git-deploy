#!/bin/sh
#
# Run git --git-dir=/var/lib/root.git --work-tree=/
#
# Written by Tony Finch <dot@dotat.at>
# You may do anything with this, at your own risk.
# http://creativecommons.org/publicdomain/zero/1.0/

GIT="git --git-dir=/var/lib/root.git --work-tree=/"

case "$1" in
init)
	exec $GIT "$@" --shared=0700
	;;
*)
	exec $GIT "$@"
	;;
esac

# eof
